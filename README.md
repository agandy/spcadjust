spcadjust 
=========
This repository contains the R-package spcadjust, developed and maintained by 
[Axel Gandy](http://wwwf.imperial.ac.uk/~agandy/) and [Jan Terje Kvaløy](http://www.ux.uis.no/~jtk/indexe.html).
If you want to contribute features or charts, please do get in touch. 

Major updates are being posted to [CRAN](https://cran.r-project.org/package=spcadjust).



Usage
---- 
Install from CRAN
```R
install.packages("spcadjust")
```

Install latest release from bitbucket (requires working and current installation of devtools).
```R
library(devtools)
install_bitbucket("agandy/spcadjust")
```

Install latest development version from bitbucket (might be unstable).
```R
library(devtools)
install_bitbucket("agandy/spcadjust",ref="develop")
```